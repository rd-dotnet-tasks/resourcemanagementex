﻿using System;
using System.IO;


namespace ResourceManagement
{
    class Resource : IDisposable
    {
        private StreamWriter streamWriter;
        bool disposed = false;

        public Resource()
        {
            this.streamWriter = File.CreateText("res.txt");
        }

        ~Resource()
        {
            Dispose(false);
        }

        public void WriteToFile()
        {
            streamWriter.WriteLine("Unmanaged resource");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Parameters:
        //   disposing:
        //     true to release both managed and unmanaged resources; false to release only unmanaged
        //     resources.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                if (streamWriter != null)
                {
                    Console.WriteLine("Cleaning up managed resources");
                    streamWriter.Dispose();
                }
            }
            Console.WriteLine("Cleaning up unmanaged resources");
            disposed = true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (Resource res = new Resource())
            {
                res.WriteToFile();
            }
            Console.ReadKey();
        }
    }
}